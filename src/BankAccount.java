

public class BankAccount {

	private int id;
	private String name;
	private double balance;
	
	public BankAccount(int id,String name,double balance){
		this.id = id;
		this.name = name;
		this.balance = balance;
	}
	
	public void deposit(double amount){
		balance += amount;
	}
	
	public void withDraw(double amount){
		balance -= amount;
	}
	
	public double getBalance(){
		return balance;
	}
}
