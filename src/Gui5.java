import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class Gui5 {
	JFrame frame ;
	JMenuBar color ;
	JMenu rgb;
	JMenuItem red;
	JMenuItem green;
	JMenuItem blue;

	JPanel buttonPanel;
	JPanel BGPanel;



	public static void main(String[] args){
		new Gui5();
	}
	public Gui5(){
		frame = new JFrame();
		frame.setSize(300,450);
		createPbutton();
		frame.setVisible(true);
	}
	public void createPbutton(){
		color = new JMenuBar();
		rgb = new JMenu("Color");
		red = new JMenuItem("Red");
		green = new JMenuItem("Green");
		blue = new JMenuItem("Blue");
		color.add(rgb);
		rgb.add(red);
		rgb.add(green);
		rgb.add(blue);
		BGPanel= new JPanel();
		frame.add(color,BorderLayout.NORTH);
		frame.add(BGPanel,BorderLayout.CENTER);
		
		red.addActionListener(new ActionListener(){

			
			@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					BGPanel.setBackground(Color.RED);
				}});
		green.addActionListener(new ActionListener(){

			
			@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					BGPanel.setBackground(Color.GREEN);
				}});
		blue.addActionListener(new ActionListener(){

			
			@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					BGPanel.setBackground(Color.BLUE);
				}});
	}
}