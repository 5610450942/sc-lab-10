import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class Gui2 {
	JFrame frame ;
    JRadioButton redButton ;
    JRadioButton greenButton ;
    ButtonGroup bG;
    JRadioButton blueButton ;
	JPanel buttonPanel;
	JPanel BGPanel;


	public static void main(String[] args){
		new Gui2();
	}
	public Gui2(){
		frame = new JFrame();
		frame.setSize(300,450);
		createPbutton();
		frame.setVisible(true);
	}
	public void createPbutton(){
		bG = new ButtonGroup();
		redButton = new JRadioButton("Red");
		greenButton = new JRadioButton("Green");
		blueButton = new JRadioButton("Blue");
		buttonPanel = new JPanel();
		BGPanel= new JPanel();

		frame.add(buttonPanel,BorderLayout.SOUTH);
		frame.add(BGPanel,BorderLayout.CENTER);

		buttonPanel.setLayout(new FlowLayout());
		bG.add(redButton);
		bG.add(greenButton);
		bG.add(blueButton);
		buttonPanel.add(redButton);
		buttonPanel.add(greenButton);
		buttonPanel.add(blueButton);

		greenButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(greenButton.isSelected() == true){
				BGPanel.setBackground(Color.GREEN);}
				
			}});
		redButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(redButton.isSelected() == true){

				BGPanel.setBackground(Color.RED);}
				
			}});
		blueButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(blueButton.isSelected() == true){

				BGPanel.setBackground(Color.BLUE);}
			}});
	}
}
