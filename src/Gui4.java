import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Gui4 {
	JFrame frame ;
    JComboBox color ;
	JPanel buttonPanel;
	JPanel BGPanel;



	public static void main(String[] args){
		new Gui4();
	}
	public Gui4(){
		frame = new JFrame();
		frame.setSize(300,450);
		createPbutton();
		frame.setVisible(true);
	}
	public void createPbutton(){
		color = new JComboBox();
		color.addItem("Red");
		color.addItem("Green");
		color.addItem("Blue");
		buttonPanel = new JPanel();
		BGPanel= new JPanel();

		frame.add(buttonPanel,BorderLayout.SOUTH);
		frame.add(BGPanel,BorderLayout.CENTER);

		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(color);
		
		color.addActionListener(new ActionListener(){

			
			@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					if(color.getSelectedItem().equals("Green")){
					BGPanel.setBackground(Color.GREEN);}
					if(color.getSelectedItem().equals("Blue")){
					BGPanel.setBackground(Color.BLUE);}
					if(color.getSelectedItem().equals("Red")){
					BGPanel.setBackground(Color.RED);}
				}});

	}
}
