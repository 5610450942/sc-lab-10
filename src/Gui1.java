import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Gui1 {
	JFrame frame ;
	JButton redButton ;
	JButton greenButton ;
	JButton blueButton ;
	JPanel buttonPanel;
	JPanel BGPanel;


	public static void main(String[] args){
		new Gui1();
	}
	public Gui1(){
		frame = new JFrame();
		frame.setSize(300,450);
		createPbutton();
		frame.setVisible(true);
	}
	public void createPbutton(){
		redButton = new JButton("Red");
		greenButton = new JButton("Green");
		blueButton = new JButton("Blue");
		buttonPanel = new JPanel();
		BGPanel= new JPanel();

		frame.add(buttonPanel,BorderLayout.SOUTH);
		frame.add(BGPanel,BorderLayout.CENTER);

		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(redButton);
		buttonPanel.add(greenButton);
		buttonPanel.add(blueButton);

		greenButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				BGPanel.setBackground(Color.GREEN);
				
			}});
		redButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				BGPanel.setBackground(Color.RED);
				
			}});
		blueButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				BGPanel.setBackground(Color.BLUE);
			}});
	}
}
