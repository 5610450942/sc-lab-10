import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class Gui3 {
	JFrame frame ;
    JCheckBox redButton ;
    JCheckBox greenButton ;
    JCheckBox blueButton ;
	JPanel buttonPanel;
	JPanel BGPanel;
	Color color;
	 int r;
	 int g;
	 int b;



	public static void main(String[] args){
		new Gui3();
	}
	public Gui3(){
		frame = new JFrame();
		r = 0;
		g = 0;
		b = 0;
		frame.setSize(300,450);
		createPbutton();
		frame.setVisible(true);
	}
	public void createPbutton(){
		redButton = new JCheckBox("Red");
		greenButton = new JCheckBox("Green");
		blueButton = new JCheckBox("Blue");
		buttonPanel = new JPanel();
		BGPanel= new JPanel();
		BGPanel.setBackground( new Color(r,g,b));

		frame.add(buttonPanel,BorderLayout.SOUTH);
		frame.add(BGPanel,BorderLayout.CENTER);

		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(redButton);
		buttonPanel.add(greenButton);
		buttonPanel.add(blueButton);

		greenButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(greenButton.isSelected() == true){
					g = 255;}
				else{g = 0;}
				BGPanel.setBackground( new Color(r,g,b));

				System.out.print("A");
			}});
		redButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(redButton.isSelected() == true){
					r= 255;}
				else{r = 0;}
				BGPanel.setBackground( new Color(r,g,b));

			}});
		blueButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(blueButton.isSelected() == true){
					b = 255;}
				else{b = 0;}
				BGPanel.setBackground( new Color(r,g,b));

			}});
	}
}
